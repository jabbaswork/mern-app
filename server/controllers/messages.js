// models/messeges.js

const mongoose = require('mongoose');

const MessegesSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  updated_date: {
    type: Date,
    default: Date.now
  }
});

module.exports = Messege = mongoose.model('messeges', MessegesSchema);