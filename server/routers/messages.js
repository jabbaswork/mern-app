const express = require('express');
const router = express.Router();

const Messeges = require('../controllers/messages')


router.get('/', (req, res) => {
  Messeges.find()
    .then(books => res.json(books))
    .catch(err => res.status(404).json({ error: 'No Messages found' }));
});


router.post('/', (req, res) => {
  console.log("req in messeges", req.body)
  Messeges.create(req.body)
    .then(book => res.json({ msg: 'Message added successfully' }))
    .catch(err => console.log("EROR : ", err));
});


router.put('/:id', (req, res) => {
  console.log("req.params.id", req.params.id)

  Messeges.findByIdAndUpdate(req.params.id, req.body)
    .then(book => {
      return res.json({ msg: "Message updated successfully" }
      )
    })
    .catch(err =>
      res.status(400).json({ error: 'Unable to update the Database' })
    );
});


router.delete('/:id', (req, res) => {
  Messeges.findByIdAndRemove(req.params.id)
    .then(book => res.json({ msg: 'Message deleted successfully' }))
    .catch(err => console.log("ERR :::", err));
});



module.exports = router;