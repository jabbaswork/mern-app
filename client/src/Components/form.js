import { Button, Form, Input } from 'antd';
import axios from 'axios'
import React, { useEffect, useRef, useState } from 'react';
import { notification } from '../utils/notiification'
const FormComponent = () => {
    const formRef = React.useRef(null);
    const ref = useRef(null);
    const [list, setList] = useState([]);
    const [title, setTitle] = useState('');
    const [loading, setLoading] = useState(false);




    useEffect(() => {
        ref.current.focus();
        getDataHandler();
    }, [])


    const getDataHandler = (values) => {
        axios.get('/messages').then((res) => {
            setList(res.data)
        })
    };



    const addHandler = () => {
        if (title) {
            setLoading(true)
            axios.post('/messages', { title })
                .then((res) => {
                    notification('success', res.data.msg);
                    getDataHandler();
                })
                .catch((err) => {
                    console.log('Error in CreateBook!');
                }).finally(() => setLoading(false));
        }

    }

    const deleteHandler = (id) => {
        axios.delete(`/messages/${id}`)
            .then((res) => {
                notification('error', res.data.msg);
                getDataHandler();
            })
            .catch((err) => {
                console.log('Error in CreateBook!');
            });


    }



    return (
        <div>
            <Form
                ref={formRef}
                name="control-ref"
                onFinish={addHandler}
            >
                <Form.Item
                    name="title"
                    label="Title"
                    rules={[
                        {
                            required: true,
                        },
                    ]}
                >
                    <Input ref={ref} onChange={(e) => setTitle(e.target.value)} />
                </Form.Item>
                <Form.Item >
                    <Button type="primary" htmlType="submit" loading={loading}>
                        Submit
                    </Button>
                </Form.Item>
            </Form>

            <div>
                {list.map(item => {
                    return <div>{item.title}<button onClick={() => deleteHandler(item._id)}>DELETE</button></div>
                })}
            </div>
        </div>
    );
};
export default FormComponent;